$(document).ready(function() {

    $('.header-slider').cycle({
        fx: 'cocacola',
        slides: '>div',
        pager: '.header-slider-pager',
        pagerTemplate: '<a href="#"></a>'
    });

    $.fn.cycle.transitions.cocacola = {
        before: function(opts, curr, next, fwd) {
            var css = opts.API.getSlideOpts(opts.nextSlide).slideCss || {};
            opts.API.stackSlides(curr, next, fwd);
            opts.cssBefore = $.extend(css, {
                opacity: 0,
                visibility: 'visible',
                display: 'block'
            });

            $(next).find('img').addClass('anima-caderno');
            $(curr).find('img').removeClass('anima-caderno');

            opts.animIn = { opacity: 1 };
            opts.animOut = { opacity: 0 };
        }
    };


    $('.outros-carousel').cycle({
        fx: 'carousel',
        timeout: 1000,
        pauseOnHover: true,
        slides: '>a'
    });


    $(".lightbox").fancybox({
        helpers: {
            title: { type: 'outside' }
        },

        maxHeight: '82%',

        afterLoad: function() {
            this.title = '<a href="' + $(this.element).data('link') + '" target="_blank">Clique para mais informações</a>';
        }
    });

});