var gulp     = require('gulp'),
    less     = require('gulp-less'),
    cssmin   = require('gulp-cssmin'),
    prefix   = require('gulp-autoprefixer'),
    jshint   = require('gulp-jshint'),
    uglify   = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    concat   = require('gulp-concat'),
    rename   = require('gulp-rename'),
    notify   = require('gulp-notify');


var config = {

    dev: {

        less    : 'development/less',
        js      : 'development/js/',
        vendor  : 'development/js/vendor',
        img     : 'development/img'

    },

    build: {

        css : 'public/assets/css',
        js  : 'public/assets/js',
        img : 'public/assets/img'

    },

    vendorPlugins: [

        'cycle2.min.js',
        'cycle2.carousel.min.js',
        'fancybox.min.js'

    ]

};


gulp.task('cssBuild', function() {

    gulp.src([config.dev.less + '/*.less', '!' + config.dev.less + '/_*.less'])
        .pipe(less())
        .on('error', function(error) {
            return notify().write('LESS: ' + error.message);
        })
        .pipe(concat('main.min.css'))
        .pipe(prefix())
        .pipe(cssmin())
        .pipe(gulp.dest(config.build.css))

});


gulp.task('jsHint', function() {

    gulp.src(config.dev.js + '/*.js')
        .pipe(jshint())
        .pipe(notify(function(file) {
            if (file.jshint.success) {
                return false;
            }

            var errors = file.jshint.results.map(function (data) {
                if (data.error) {
                    return data.error.reason + ' (line ' + data.error.line + ')';
                }
            }).join('\n');

            return 'JSHINT: ' + file.relative + ' (' + file.jshint.results.length + ' errors)\n' + errors;
        }));

});


gulp.task('jsBuild', function() {

    gulp.src(config.dev.js + '/*.js')
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .on('error', function(error) {
            return notify().write('UGLIFY: ' + error.message + ' (' + error.fileName + ', line: ' + error.lineNumber + ')');
        })
        .pipe(gulp.dest(config.build.js))

});


gulp.task('vendor', function() {

    for (var i=0 ; i < config.vendorPlugins.length; i++) {
        config.vendorPlugins[i] = config.dev.vendor + '/' + config.vendorPlugins[i];
    }

    gulp.src(config.vendorPlugins)
        .pipe(concat('vendor.min.js'))
        .pipe(uglify())
        .on('error', function(error) {
            return notify().write('UGLIFY: ' + error.message + ' (' + error.fileName + ', line: ' + error.lineNumber + ')');
        })
        .pipe(gulp.dest(config.build.js))

});


gulp.task('images', function() {

    gulp.src(config.dev.img + '/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest(config.build.img));

});


gulp.task('watch', function() {

    gulp.watch(config.dev.less + '/**/*.less', ['cssBuild']);
    gulp.watch(config.dev.js + '/**/*.js', ['jsHint', 'jsBuild']);

});


gulp.task('default', ['cssBuild', 'jsHint', 'jsBuild', 'watch']);