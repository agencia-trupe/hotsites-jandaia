$(document).ready(function() {

    $(".lightbox").fancybox({
        helpers: {
            title: { type: 'outside' }
        },
        maxHeight: '82%',
        afterLoad: function() {
            this.title = '<a href="' + $(this.element).data('link') + '" target="_blank">Clique para mais informações</a>';
        }
    });

});