var gulp     = require('gulp'),
    less     = require('gulp-less'),
    cssmin   = require('gulp-cssmin'),
    prefix   = require('gulp-autoprefixer'),
    jshint   = require('gulp-jshint'),
    uglify   = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    concat   = require('gulp-concat'),
    rename   = require('gulp-rename'),
    notify   = require('gulp-notify');


var config = {

    dev: {

        less : 'assets/css/_dev',
        js   : 'assets/js/_dev',
        img  : 'assets/img/_original'

    },

    build: {

        css : 'assets/css',
        js  : 'assets/js',
        img : 'assets/img'

    }

};


gulp.task('cssBuild', function() {

    gulp.src(config.dev.less + '/*.less')
        .pipe(less())
        .on('error', function(error) {
            return notify().write('LESS: ' + error.message);
        })
        .pipe(concat('main.css'))
        .pipe(prefix())
        .pipe(gulp.dest(config.build.css))
        .pipe(cssmin())
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest(config.build.css))

});


gulp.task('jsHint', function() {

    gulp.src(config.dev.js + '/**/*.js')
        .pipe(jshint())
        .pipe(notify(function(file) {
            if (file.jshint.success) {
                return false;
            }

            var errors = file.jshint.results.map(function (data) {
                if (data.error) {
                    return data.error.reason + ' (line ' + data.error.line + ')';
                }
            }).join('\n');

            return 'JSHINT: ' + file.relative + ' (' + file.jshint.results.length + ' errors)\n' + errors;
        }));

});


gulp.task('jsBuild', function() {

    gulp.src(config.dev.js + '/**/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest(config.build.js))
        .pipe(uglify())
        .on('error', function(error) {
            return notify().write('UGLIFY: ' + error.message + ' (' + error.fileName + ', line: ' + error.lineNumber + ')');
        })
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest(config.build.js))

});


gulp.task('images', function() {

    gulp.src(config.dev.img + '/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest(config.build.img));

});


gulp.task('watch', function() {

    gulp.watch(config.dev.less + '/**/*.less', ['cssBuild']);
    gulp.watch(config.dev.js + '/**/*.js', ['jsHint', 'jsBuild']);

});


gulp.task('default', ['cssBuild', 'jsHint', 'jsBuild', 'watch']);